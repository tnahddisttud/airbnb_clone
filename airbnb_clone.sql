-- Create a database
create database airbnb_clone;
use airbnb_clone;
-- create users table
create table users(id int primary key auto_increment, 
				   name varchar(25), 
                   email varchar(25),
                   phone_number varchar(14),
                   bio text);
-- Inserting dummmy data into the users table
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('1', 'Ajay', 'ajay@test.com', '+91-7321394347', 'Just another traveller');
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('2', 'Ankit', 'ankit@test.com', '+91-3721398463', 'blah blah blah');
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('3', 'Shraddha', 'shra@test.com', '+91-1837721398', 'Hello world');
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('4', 'Dhruv', 'dhr@test.com', '+91-7325436355', 'Just another traveller');
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('5', 'Rajesh', 'raj@test.com', '+91-3721242131', 'blah blah blah');
INSERT INTO `airbnb_clone`.`users` (`id`, `name`, `email`, `phone_number`, `bio`) VALUES ('6', 'Meha', 'meha@test.com', '+91-1837713213', 'Hello world');

-- using the alter clause
alter table users rename column id to user_id;

-- create rooms table

create table rooms(room_id int primary key auto_increment,
				   home_type varchar(20),
                   total_occupancy int,
                   total_bedrooms int,
                   summary text,
                   address text,
                   has_tv boolean,
                   has_internet boolean,
                   price int,
                   owner_id int,
                   foreign key(owner_id) references users(user_id)
);

-- Inserting rooms data
INSERT INTO `airbnb_clone`.`rooms` (`room_id`, `home_type`, `total_occupancy`, `total_bedrooms`, `summary`, `address`, `has_tv`, `has_internet`, `price`, `owner_id`) VALUES ('10', 'Private room', '2', '1', 'Chill out and relax in a cozy room with great view', 'Udaipur, Rajasthan', 1 , 0 , '900', '3');
INSERT INTO `airbnb_clone`.`rooms` (`room_id`, `home_type`, `total_occupancy`, `total_bedrooms`, `summary`, `address`, `has_tv`, `has_internet`, `price`, `owner_id`) VALUES ('11', 'Hotel room', '4', '3', 'Relax with your family in a blissful environment', 'Goa', 1,1, '1500', '2');
INSERT INTO `airbnb_clone`.`rooms` (`room_id`, `home_type`, `total_occupancy`, `total_bedrooms`, `summary`, `address`, `has_tv`, `has_internet`, `price`, `owner_id`) VALUES ('12', 'Entire Bungalow', '8', '4', 'Away from the bustle of the crowded area, suitable place to kickback and relax', 'Mahabaleshwar, maharastra', 1, 1, '2200', '6');

-- Creating Reservations table
create table reservations(res_id int primary key auto_increment,
						  user_id int,
                          room_id int,
                          start_date datetime,
                          end_date datetime,
                          price int,
                          total int,
                          foreign key(user_id) references users(user_id),
                          foreign key(room_id) references rooms(room_id)
);

INSERT INTO `airbnb_clone`.`reservations` (`res_id`, `user_id`, `room_id`, `start_date`, `end_date`, `price`, `total`) VALUES ('111', '1', '12', STR_TO_DATE('01-01-2022', '%d-%m-%Y'),STR_TO_DATE('03-01-2022', '%d-%m-%Y'), '2200', '6600');
INSERT INTO `airbnb_clone`.`reservations` (`res_id`, `user_id`, `room_id`, `start_date`, `end_date`, `price`, `total`) VALUES ('112', '4', '10', STR_TO_DATE('25-12-2021', '%d-%m-%Y'),STR_TO_DATE('01-01-2022', '%d-%m-%Y'), '900', '4500');
INSERT INTO `airbnb_clone`.`reservations` (`res_id`, `user_id`, `room_id`, `start_date`, `end_date`, `price`, `total`) VALUES ('113', '5', '11', STR_TO_DATE('12-02-2022', '%d-%m-%Y'),STR_TO_DATE('14-02-2022', '%d-%m-%Y') , '1500', '4500');

-- creating reviews table and inserting values
create table reviews(reviews_id int primary key auto_increment,
					 res_id int,
                     rating int,
                     comment varchar(250),
                     foreign key(res_id) references reservations(res_id),
                     check(0<rating<=5)
);

INSERT INTO `airbnb_clone`.`reviews` (`reviews_id`, `res_id`, `rating`, `comment`) VALUES ('1', '113', '5', 'Great place and great service');
INSERT INTO `airbnb_clone`.`reviews` (`reviews_id`, `res_id`, `rating`, `comment`) VALUES ('2', '111', '4', 'Nice room');
INSERT INTO `airbnb_clone`.`reviews` (`reviews_id`, `res_id`, `rating`, `comment`) VALUES ('3', '112', '3', 'service not good');

-- writing queries using aggregate functions and clauses

-- Find the room with minimum price
select * from rooms where price = (select min(price) from rooms);

-- Find the room with maximum price (using 'in' clause)
select * from rooms where price in (select max(price) from rooms);

-- Count reservations with total price <5000
select count(res_id) from reservations where total < 5000;

-- Display reservations with total price >5000
select * from reservations where total > 5000 order by price desc;

-- Find average price of rooms which have internet/wifi
select avg(price) from rooms where has_internet = true;

-- Find the total income generated by reservations
select sum(total) from reservations;

-- display count of wether or not the rooms have internet
select has_internet, count(*) as Number  from rooms group by has_internet;

-- Using REGEX clause
-- List users with name A
select * from users where name regexp '^A';

-- Joins
-- Display users who own a house and save it as 'Owners view'
create view owners as
	select user_id, users.name, rooms.room_id, rooms.home_type
		from users,rooms
		where users.user_id = rooms.owner_id
        order by user_id;

-- Display users and their reservations
select * from users join reservations on reservations.user_id = users.user_id;

-- Display users and their reviews (Eg of multiple joins)
select users.name, reviews.comment from users join reservations on users.user_id = reservations.user_id join reviews on reservations.res_id = reviews.res_id;

-- stored procedures
delimiter $$
create procedure FindBedrooms(in num int)
begin
select * from rooms where total_bedrooms = num order by room_id;
end$$
delimiter ;

call FindBedrooms(3);

start transaction;
insert into rooms values('13','Hotel room', '5', '3', 'Comfortable room with sea view', 'Goa', 1,1, '1800', '2');
select * from rooms;
commit;









